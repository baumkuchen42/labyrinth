# Labyrinth

An example application of how intelligent agents can clone themselves to collectively solve a labyrinth.

The cloning is done via thread creation for each fork in the labyrinth.

![image](/uploads/49dce401c7fe4848200d55fe9b518e32/image.png)
