# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import random

from gi.repository import Gtk, Gdk

from threading import Thread

from .solver_agent import SolverAgent, Direction
from .timer import timeout


@Gtk.Template(resource_path='/de/hszg/labyrinth/window.ui')
class LabyrinthTask3Window(Gtk.ApplicationWindow):
	__gtype_name__ = 'LabyrinthTask3Window'

	main_grid = Gtk.Template.Child()
	mode_select = Gtk.Template.Child()
	start_stop_btn = Gtk.Template.Child()
	start_stop_img = Gtk.Template.Child()
	x_dimension_entry = Gtk.Template.Child()
	y_dimension_entry = Gtk.Template.Child()
	random_generator_btn = Gtk.Template.Child()

	_target_reached = False
	_stopped = None

	thread_count = 0
	waiting_time = 0.1

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.load_css()
		self.fill_grid_with_buttons()
		self.connect_events()

	def fill_grid_with_buttons(self, widget=None):
		x_dimension = int(self.x_dimension_entry.get_text())
		y_dimension = int(self.y_dimension_entry.get_text())
		for existing_child in self.main_grid.get_children():
			self.main_grid.remove(existing_child)
		for x in range(x_dimension):
			for y in range(y_dimension):
				btn = Gtk.Button()
				self.main_grid.attach(btn, x, y, 1, 1)
				btn.connect('clicked', self.set_btn_kind)
		self.main_grid.show_all()


	def connect_events(self):
		self.start_stop_btn.connect('clicked', self.toggle_start_stop)
		self.x_dimension_entry.connect('activate', self.fill_grid_with_buttons)
		self.y_dimension_entry.connect('activate', self.fill_grid_with_buttons)
		self.random_generator_btn.connect('clicked', self.generate_random_labyrinth)

	def set_btn_kind(self, btn):
		btn_style = btn.get_style_context()
		style = self.mode_select.get_active_id()
		if btn_style.has_class(style):
			btn_style.remove_class(style)
			return
		if style == 'start' or style == 'end':
			for child in self.main_grid.get_children():
				child.get_style_context().remove_class(style)
		btn_style.add_class(style)

	def generate_random_labyrinth(self, trigger_btn=None):
		x_dim = int(self.x_dimension_entry.get_text())
		y_dim = int(self.y_dimension_entry.get_text())
		self.fill_grid_with_buttons()
		self.select_random_start_and_end(x_dim, y_dim)
		weights = [0.6, 0.4] if (x_dim <= 10 and y_dim <= 10) else [0.4, 0.6] # TODO maybe make more proportional to dimensions
		for btn in self.main_grid.get_children():
			if (
				not btn.get_style_context().has_class('end')
				and not btn.get_style_context().has_class('start')
				):
					style = random.choices(['', 'obstacle'], weights=weights)[0]
					btn.get_style_context().add_class(style)


	def select_random_start_and_end(self, x_dim, y_dim):
		start = (random.randint(0, x_dim-1), random.randint(0, y_dim-1))
		self.main_grid.get_child_at(start[0], start[1]).get_style_context().add_class('start')

		end = (random.randint(0, x_dim-1), random.randint(0, y_dim-1))
		self.main_grid.get_child_at(end[0], end[1]).get_style_context().add_class('end')

	def toggle_start_stop(self, start_stop_btn=None):
		self.clear_agents_from_gui()
		if self.start_stop_img.get_icon_name()[0] == 'media-playback-start-symbolic':
			self._stopped = False
			start = self.get_position_by_style('start')
			if start and self.get_position_by_style('end'):
				self.init_target_search(start)
			else:
				self.notify('Please set a starting point and an end point for your labyrinth before you click start.')
		else:
			self._stopped = True
			self.start_stop_img.set_from_icon_name('media-playback-start-symbolic', Gtk.IconSize.BUTTON)

	def init_target_search(self, start):
		agent_thread = SolverAgent(self, start, old_position=None)
		agent_thread.start()
		self.thread_count += 1
		self.start_stop_img.set_from_icon_name('media-playback-stop-symbolic', Gtk.IconSize.BUTTON)

	def clear_agents_from_gui(self):
		for btn in self.main_grid.get_children():
				btn.get_style_context().remove_class('suggested-action')

	def get_position_by_style(self, style:str):
		for btn in self.main_grid.get_children():
			if btn.get_style_context().has_class(style):
				x = self.main_grid.child_get_property(btn, 'left-attach')
				y = self.main_grid.child_get_property(btn, 'top-attach')
				return (x, y)

	def update_agent_position(self, old_position:tuple, new_position:tuple):
		old = self.main_grid.get_child_at(old_position[0], old_position[1])
		old.get_style_context().remove_class('suggested-action')
		new = self.main_grid.get_child_at(new_position[0], new_position[1])
		new.get_style_context().add_class('suggested-action')

	def is_occupied_already(self, position:tuple):
		btn = self.main_grid.get_child_at(position[0], position[1])
		return btn.get_style_context().has_class('suggested-action')

	def remove_agent_from_gui(self, position:tuple):
		old = self.main_grid.get_child_at(position[0], position[1])
		old.get_style_context().remove_class('suggested-action')

	def add_agent_to_gui(self, position:tuple):
		new = self.main_grid.get_child_at(position[0], position[1])
		new.get_style_context().add_class('suggested-action')

	def clone_agent(self, position:tuple, old_position:tuple):
		print(self.thread_count)
		if self.stopped:
			return
		# if self.thread_count % 20 == 0 and self.thread_count != 0:
		# 	self.waiting_time += self.waiting_time * 0.1
		self.thread_count += 1
		clone = SolverAgent(self, position, old_position)
		clone.start()

	@property
	def target_reached(self):
		return self._target_reached

	@property
	def stopped(self):
		return self._stopped

	@target_reached.setter
	def target_reached(self, reached_and_position:tuple):
		if not self.target_reached:
			reached, position = reached_and_position
			self._target_reached = reached
			winner = self.main_grid.get_child_at(position[0], position[1])
			winner.get_style_context().add_class('winner')
			self.notify('The Labyrinth has been solved!')
			self.make_restartable()

	def make_restartable(self):
		self.toggle_start_stop()
		winner = self.get_position_by_style('winner')
		winner_btn = self.main_grid.get_child_at(winner[0], winner[1])
		winner_btn.get_style_context().remove_class('winner')
		self._target_reached = False
		self.waiting_time = 0.1

	def abort_target_search(self):
		if not self.target_reached:
			self._stopped = True
			self.clear_agents_from_gui()
			self.notify('The search is taking too long. Aborting.')
			self.toggle_start_stop()

	def notify(self, text):
		msg = Gtk.MessageDialog(
			transient_for=self,
			message_type=Gtk.MessageType.INFO,
			buttons=Gtk.ButtonsType.OK,
			text=text
		)
		msg.run()
		msg.destroy()


	def load_css(self):
		cssProvider = Gtk.CssProvider()
		cssProvider.load_from_resource('/de/hszg/labyrinth/style.css')
		screen = Gdk.Screen.get_default()
		styleContext = Gtk.StyleContext()
		styleContext.add_provider_for_screen(screen, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_USER)
