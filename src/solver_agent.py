import operator, time, traceback

from gi.repository import GLib

from enum import Enum
from threading import Thread

class Direction(Enum):
	UP = (0, -1)
	DOWN = (0, 1)
	LEFT = (-1, 0)
	RIGHT = (1, 0)

class SolverAgent(Thread):
	def __init__(self, gui, position:tuple, old_position:tuple):
		super().__init__()
		self.gui = gui
		self.position = position
		self.old_position = old_position
		self.abort = False
		GLib.idle_add(self.gui.add_agent_to_gui, self.position)

	def run(self):
		if self.abort or self.gui.stopped:
			return

		elif self.has_reached_target():
			self.notify_gui_of_target_reach()
			return

		else:
			self.move()


	def detect_obstacle(self, position):
		next_btn = self.gui.main_grid.get_child_at(position[0], position[1])
		if next_btn is None:
			return True
		if next_btn.get_style_context().has_class('obstacle'):
			return True

	def has_reached_target(self):
		position_btn = self.gui.main_grid.get_child_at(self.position[0], self.position[1])
		if position_btn is None:
			print('am in nothing')
		if position_btn.get_style_context().has_class('end'):
			return True

	def move(self):
		time.sleep(self.gui.waiting_time)
		GLib.idle_add(self.gui.remove_agent_from_gui, self.position)
		self.gui.thread_count -= 1

		for direction in Direction:
			new_position = self.add(self.position, direction)
			if (
				new_position != self.old_position
				and not self.detect_obstacle(new_position)
				and not self.gui.target_reached
				and not self.gui.is_occupied_already(new_position)
				):
					GLib.idle_add(self.gui.clone_agent, new_position, self.position)
			time.sleep(self.gui.waiting_time)

	def notify_gui_of_target_reach(self):
		self.gui.target_reached =  (True, self.position)

	def add(self, position:tuple, direction:Direction):
		return tuple(map(operator.add, position, direction.value))
